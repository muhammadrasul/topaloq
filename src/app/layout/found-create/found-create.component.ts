import { Component, OnInit, OnDestroy } from '@angular/core';
declare var $;

@Component({
  selector: 'app-found-create',
  templateUrl: './found-create.component.html',
  styleUrls: ['./found-create.component.scss']
})
export class FoundCreateComponent implements OnInit, OnDestroy {

  constructor() { }

  ngOnInit() {
    $('.navbar').css("background-color", "#4285F4");
    $('.navbar .collapse .nav-item .dropdown-menu').css("background-color", "#4285F4");
  }

  ngOnDestroy() {
    $('.navbar').css("background-color", "");
    $('.navbar .collapse .nav-item .dropdown-menu').css("background-color", "");
  }
}
