import { Component, OnInit } from '@angular/core';
import { LocalStorageSecurity } from '../../dto/localStorageSecurity';
import { CommonKey } from '../../dto/commonKey';
import { Router } from '@angular/router';

@Component({
  selector: 'app-my-profile',
  templateUrl: './my-profile.component.html',
  styleUrls: ['./my-profile.component.scss']
})
export class MyProfileComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
    if (!LocalStorageSecurity.hasItem(CommonKey.TOKEN)) {
      if (location.pathname.split('/')[1] === 'ru') {
        this.router.navigate(["ru"]);
      } else {
        this.router.navigate([""]);
      }

      
    }
  }

}
