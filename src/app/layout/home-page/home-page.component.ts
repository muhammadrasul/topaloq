import { Component, OnInit } from '@angular/core';
import { LocalStorageSecurity } from '../../dto/localStorageSecurity';
import { CommonKey } from '../../dto/commonKey';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {

  public isRussian: boolean = false;

  constructor(private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    if (location.pathname.split('/')[1] === 'ru') {
      this.isRussian = true;
    }
  }

  public foundSmth() {
    if (LocalStorageSecurity.hasItem(CommonKey.TOKEN)) {
      this.router.navigate(["i-found"], {relativeTo: this.route});
    } else {
      document.getElementById("findOpener").click();
    }
  }

  public lostSmth() {
    if (LocalStorageSecurity.hasItem(CommonKey.TOKEN)) {
      this.router.navigate(["i-lost"], {relativeTo: this.route});
    } else {
      document.getElementById("lostOpener").click();
    }
  }
}
