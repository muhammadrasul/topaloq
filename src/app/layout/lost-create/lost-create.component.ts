import { Component, OnInit, OnDestroy } from '@angular/core';
import { MouseEvent } from '@agm/core';
import { LostItemDTO } from '../../dto/lostItemDto';
import { LocalStorageSecurity } from '../../dto/localStorageSecurity';
import { CommonKey } from '../../dto/commonKey';
import { SubjectService } from '../../services/subject.service';
import { TypeDTO } from '../../dto/typeDto';
declare var $;

interface marker {
	lat: number;
	lng: number;
	label?: string;
	draggable: boolean;
}

@Component({
  selector: 'app-lost-create',
  templateUrl: './lost-create.component.html',
  styleUrls: ['./lost-create.component.scss'],
  providers: [ SubjectService ]
})
export class LostCreateComponent implements OnInit, OnDestroy {

  public isRussian: boolean = false;
  public zoom: number = 13;
  public lat: number = 41.3074;
  public lng: number = 69.2312;
  public nomi: string = '';
  public date: Date = new Date();
  public markers;
  public firstName: string;
  public lastName: string;
  public types: Array<TypeDTO>;
  public regions: Array<string>;

  constructor(private subjectService: SubjectService) {
    this.markers = [];
    this.types = [];
  }

  ngOnInit() {
    if (location.pathname.split('/')[1] === 'ru') {
      this.isRussian = true;
    }
    if (LocalStorageSecurity.hasItem(CommonKey.TOKEN)) {
      this.firstName = LocalStorageSecurity.getItem(CommonKey.NAME);
      this.lastName = LocalStorageSecurity.getItem(CommonKey.SURNAME);
    }
    $('.navbar').css("background-color", "#4285F4");
    $('.navbar .collapse .nav-item .dropdown-menu').css("background-color", "#4285F4");

    this.getSubjectTypes();
    this.getRegions();
  }

  ngOnDestroy() {
    $('.navbar').css("background-color", "");
    $('.navbar .collapse .nav-item .dropdown-menu').css("background-color", "");
  }

  public register() {
    var lostItem = new LostItemDTO();
    lostItem.name = this.nomi;
    lostItem.date = (<HTMLInputElement>document.getElementById("lostDate")).value;
    console.log(lostItem);
  }

  private getSubjectTypes() {
    this.subjectService.getSubjectTypes().subscribe(
      (data) => {
        this.types = this.types.concat(data);
        var i = 0;
        if (this.isRussian) {
          for (let x of this.subjectService.getTypesRu()) {
            this.types[i].name = x;
            i++;
          }
        } else {
          for (let x of this.subjectService.getTypesUz()) {
            this.types[i].name = x;
            i++;
          }
        }
      },
      error => console.log(error)
    );
  }

  private getRegions() {
    var lang: string;
    if (this.isRussian) {
      lang = "ru"
    } else {
      lang = "en"
    }
    this.subjectService.getRegionsList(lang).subscribe(
      (data) => {
        console.log(data);
        
      },
      error => console.log(error)
    );
  }













  clickedMarker(label: string, index: number) {
    console.log(`clicked the marker: ${label || index}`)
  }
  
  mapClicked($event: MouseEvent) {
    this.markers.shift();
    this.markers.push({
      lat: $event.coords.lat,
      lng: $event.coords.lng,
      draggable: true
    });
  }
  
  markerDragEnd(m: marker, $event: MouseEvent) {
    console.log('dragEnd', m, $event);
  }
}
