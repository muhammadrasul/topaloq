import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { GeneralURL } from '../dto/generalUrl';
import { LocalStorageSecurity } from '../dto/localStorageSecurity';
import { CommonKey } from '../dto/commonKey';
import { CryptoSecurity } from '../dto/cryptoSecurity';

import { ProfileDto } from '../dto/profileDto';
import { CountDto } from '../dto/countDto';
import { ImageDto } from '../dto/imageDto';

@Injectable()
export class ProfileService {

  constructor(private http: HttpClient) { }

  public authorization(profile: ProfileDto) {
    let json = JSON.stringify(profile);
    
    let options = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };

    return this.http.post<ProfileDto>(GeneralURL.welcomeURL.concat('/auth'), json, options);
  }

  public registration(profile: ProfileDto) {
    let json = JSON.stringify(profile);
    
    let options = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };

    return this.http.post<ProfileDto>(GeneralURL.welcomeURL.concat('reg_s_user'), json, options);
  }

  public UpdateWelcome(profile: ProfileDto) {
    let json = JSON.stringify(profile);
    
    let options = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'authorization': LocalStorageSecurity.getItem(CommonKey.TOKEN)
        }),
    };

    return this.http.put<CountDto>(GeneralURL.profileURL.concat('u_log_pswd'), json, options);
  }

  public UpdateProfile(profile: ProfileDto) {
    let json = JSON.stringify(profile);
    
    let options = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'authorization': LocalStorageSecurity.getItem(CommonKey.TOKEN)
        }),
    };

    return this.http.put<CountDto>(GeneralURL.profileURL.concat('u_detail'), json, options);
  }

  public deleteProfile(id: number) {
    let options = {
      headers: new HttpHeaders({
          'Content-Type': 'application/json',
        //   'authorization': this.getToken()
      })
    };

    return this.http.delete(GeneralURL.profileURL.concat('delete/' + id), options);
  }

  public updateMyImage(profile: ProfileDto) {
    let json = JSON.stringify(profile);
    
    let options = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json',
            'authorization': LocalStorageSecurity.getItem(CommonKey.TOKEN)
        })
    };

    return this.http.put(GeneralURL.profileURL.concat('update_img'), json, options);
  }

  public getProfiles(count: CountDto) {
    let json = JSON.stringify(count);
    
    let options = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json',
            // 'authorization': this.getToken()
        })
    };

    return this.http.post(GeneralURL.profileURL.concat('pagination'), json, options);
  }

  public createImage(file: File) {
      let formData = new FormData();
      formData.append('file', file);

      return this.http.post<ImageDto>(GeneralURL.imageURL.concat('create'), formData);
  }
}