import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { GeneralURL } from '../dto/generalUrl';
import { LocalStorageSecurity } from '../dto/localStorageSecurity';
import { CommonKey } from '../dto/commonKey';
import { CryptoSecurity } from '../dto/cryptoSecurity';

import { CountDto } from '../dto/countDto';
import { TypeDTO } from '../dto/typeDto';

@Injectable()
export class SubjectService {

  constructor(private http: HttpClient) { }

  public getTypesUz() {
    return ["Dokument", "Elektronika", "Sekurity", "Transport vositasi", "Hayvon", "Kiyim", "Tilla buyumlar", "boshqa"]
  }

  public getTypesRu() {
    return ["Документ", "Електроника", "Секурити", "Транспорт", "Hayvon", "Kiyim", "Золото", "другой"]
  }

  public getSubjectTypes() {

    let options = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'authorization': LocalStorageSecurity.getItem(CommonKey.TOKEN)
        }),
    };

    return this.http.get<TypeDTO>(GeneralURL.typeURL.concat('all'), options);
  }

  public getRegionsList(lang: string) {

    let options = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'authorization': LocalStorageSecurity.getItem(CommonKey.TOKEN)
        }),
    };

    return this.http.get<TypeDTO>(GeneralURL.locationURL.concat(lang + '/region'), options);
  }


}