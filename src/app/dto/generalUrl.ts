export class GeneralURL {
    public static welcomeURL:string = 'http://localhost:8080/topaloq/welcome/';
    public static profileURL:string = 'http://localhost:8080/topaloq/profile/';
    public static typeURL:string = 'http://localhost:8080/topaloq/subject_type/';
    public static locationURL:string = 'http://localhost:8080/topaloq/location/';
    public static imageURL:string = 'http://localhost:8080/topaloq/profile/';
}