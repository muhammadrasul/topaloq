import { BrowserModule } from '@angular/platform-browser';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { AppRoutingModule } from "./appRouting.module";
import { FormsModule }   from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AgmCoreModule } from "@agm/core";

import { AppComponent } from './app.component';
import { LogInComponent } from './administrator/log-in/log-in.component';
import { AdministratorComponent } from './administrator/administrator.component';
import { LayoutComponent } from './layout/layout.component';
import { HomePageComponent } from './layout/home-page/home-page.component';
import { FoundCreateComponent } from './layout/found-create/found-create.component';
import { LostCreateComponent } from './layout/lost-create/lost-create.component';
import { MyProfileComponent } from './layout/my-profile/my-profile.component';

@NgModule({
  declarations: [
    AppComponent,
    LogInComponent,
    AdministratorComponent,
    LayoutComponent,
    HomePageComponent,
    FoundCreateComponent,
    LostCreateComponent,
    MyProfileComponent
  ],
  imports: [
    BrowserModule,
    MDBBootstrapModule.forRoot(),
    AppRoutingModule,
    FormsModule,
    AgmCoreModule.forRoot({apiKey: 'AIzaSyBDaNo28mD5SXWHDdlmeEUdNDTbyqmNc7Q'}),
    HttpClientModule
  ],
  schemas: [ NO_ERRORS_SCHEMA ],
  providers: [
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
