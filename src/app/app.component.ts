import { Component, HostListener } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'app';

  // @HostListener('window:beforeunload', ['$event'])beforeUnloadHander(event) {
  //   localStorage.clear();
  // }
}
