import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LayoutComponent } from './layout/layout.component';
import { HomePageComponent } from './layout/home-page/home-page.component';
import { FoundCreateComponent } from './layout/found-create/found-create.component';
import { LostCreateComponent } from './layout/lost-create/lost-create.component';

import { AdministratorComponent } from './administrator/administrator.component';
import { LogInComponent } from './administrator/log-in/log-in.component';
import { MyProfileComponent } from './layout/my-profile/my-profile.component';

const appRoutes: Routes = [
    { path: '', component: LayoutComponent, children: [
        { path: '', component: HomePageComponent },
        { path: 'my-profile', component: MyProfileComponent },
        { path: 'i-found', component: FoundCreateComponent },
        { path: 'i-lost', component: LostCreateComponent }
    ]},
    { path: 'ru', component: LayoutComponent, children: [
        { path: '', component: HomePageComponent },
        { path: 'my-profile', component: MyProfileComponent },
        { path: 'i-found', component: FoundCreateComponent },
        { path: 'i-lost', component: LostCreateComponent }
    ]},
    { path: 'administrator', component: AdministratorComponent },
    { path: 'log-in', component: LogInComponent }
];

@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes)
    ],
    exports: [RouterModule],
    providers: [],
    declarations: []
})

export class AppRoutingModule {
    constructor() { }
}